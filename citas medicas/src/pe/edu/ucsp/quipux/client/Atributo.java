package pe.edu.ucsp.quipux.client;

public class Atributo{
	int cambio_simbolo = 0;
	int posiciones[] = new int[9];

	public Atributo(){
		cambio_simbolo = 0;		
		for(int i = 0; i < 9; i++)
			posiciones[i] = 0;
	}
	
	public void clear(){
		for(int i = 0; i < 9; i++)
			posiciones[i] = 0;
	}
	
	public boolean draw(){
		for(int i = 0; i < 9; i++)
			if(posiciones[i] == 0) return false;
		return true;
	}
}
