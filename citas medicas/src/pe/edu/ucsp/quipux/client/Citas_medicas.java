package pe.edu.ucsp.quipux.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */

public class Citas_medicas implements EntryPoint {	
	@Override
	public void onModuleLoad() {
		final String x = "X";
		final String o = "O";
		
		final Atributo tri = new Atributo();
				
	    Button btn1 = new Button(" ");
		Button btn2 = new Button(" ");
		Button btn3 = new Button(" ");
		Button btn4 = new Button(" ");
		Button btn5 = new Button(" ");
		Button btn6 = new Button(" ");
		Button btn7 = new Button(" ");
		Button btn8 = new Button(" ");
		Button btn9 = new Button(" ");
		Button l = new Button("Limpiar");
		
		final TextBox textBox = new TextBox();
		final TextBox nom1 = new TextBox();
		final TextBox nom2 = new TextBox();
		
		textBox.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
				if (!textBox.getText().isEmpty()) {
				}
			}
		});
		
		btn1.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				RootPanel.get("pos11").clear();
				if(tri.cambio_simbolo == 0){
					RootPanel.get("pos11").add(new HTML(x));
					tri.posiciones[0] = 1;
					tri.cambio_simbolo = 1;
				}
				else{
					RootPanel.get("pos11").add(new HTML(o));
					tri.posiciones[0] = 2;
					tri.cambio_simbolo = 0;
				}
				
				if((tri.posiciones[0] == 1 && tri.posiciones[1] == 1 && tri.posiciones[2] == 1)||
				   (tri.posiciones[0] == 1 && tri.posiciones[3] == 1 && tri.posiciones[6] == 1)||
				   (tri.posiciones[0] == 1 && tri.posiciones[4] == 1 && tri.posiciones[8] == 1)){
					Window.alert("Jugdor 1 gana!");
				}
				if((tri.posiciones[0] == 2 && tri.posiciones[1] == 2 && tri.posiciones[2] == 2)||
				   (tri.posiciones[0] == 2 && tri.posiciones[3] == 2 && tri.posiciones[6] == 2)||
				   (tri.posiciones[0] == 2 && tri.posiciones[4] == 2 && tri.posiciones[8] == 2)){
					Window.alert("Jugdor 2 gana!");
				}
				else if(tri.draw()) Window.alert("Empate!");
			}
		});
		btn2.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				RootPanel.get("pos22").clear();
				if(tri.cambio_simbolo == 0){
					RootPanel.get("pos22").add(new HTML(x));
					tri.posiciones[1] = 1;
					tri.cambio_simbolo = 1;
				}
				else{
					RootPanel.get("pos22").add(new HTML(o));
					tri.posiciones[1] = 2;
					tri.cambio_simbolo = 0;
				}
				
				if((tri.posiciones[0] == 1 && tri.posiciones[1] == 1 && tri.posiciones[2] == 1)||
				   (tri.posiciones[1] == 1 && tri.posiciones[4] == 1 && tri.posiciones[7] == 1)){
					Window.alert("Jugdor 1 gana!");
				}
				if((tri.posiciones[0] == 2 && tri.posiciones[1] == 2 && tri.posiciones[2] == 2)||
				   (tri.posiciones[1] == 2 && tri.posiciones[4] == 2 && tri.posiciones[7] == 2)){
					Window.alert("Jugdor 2 gana!");
				}
				else if(tri.draw()) Window.alert("Empate!");
			}
		});
		btn3.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				RootPanel.get("pos33").clear();
				if(tri.cambio_simbolo == 0){
					RootPanel.get("pos33").add(new HTML(x));
					tri.posiciones[2] = 1;
					tri.cambio_simbolo = 1;
				}
				else{
					RootPanel.get("pos33").add(new HTML(o));
					tri.posiciones[2] = 2;
					tri.cambio_simbolo = 0;
				}
				
				if((tri.posiciones[0] == 1 && tri.posiciones[1] == 1 && tri.posiciones[2] == 1)||
				   (tri.posiciones[2] == 1 && tri.posiciones[4] == 1 && tri.posiciones[6] == 1)||
				   (tri.posiciones[2] == 1 && tri.posiciones[5] == 1 && tri.posiciones[8] == 1)){
					Window.alert("Jugdor 1 gana!");
				}
				if((tri.posiciones[0] == 2 && tri.posiciones[1] == 2 && tri.posiciones[2] == 2)||
				   (tri.posiciones[2] == 2 && tri.posiciones[4] == 2 && tri.posiciones[6] == 2)||
				   (tri.posiciones[2] == 2 && tri.posiciones[5] == 2 && tri.posiciones[8] == 2)){
					Window.alert("Jugdor 2 gana!");
				}
				else if(tri.draw()) Window.alert("Empate!");
			}
		});
		btn4.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				RootPanel.get("pos44").clear();
				if(tri.cambio_simbolo == 0){
					RootPanel.get("pos44").add(new HTML(x));
					tri.posiciones[3] = 1;
					tri.cambio_simbolo = 1;
				}
				else{
					RootPanel.get("pos44").add(new HTML(o));
					tri.posiciones[3] = 2;
					tri.cambio_simbolo = 0;
				}
				
				if((tri.posiciones[0] == 1 && tri.posiciones[3] == 1 && tri.posiciones[6] == 1)||
				   (tri.posiciones[3] == 1 && tri.posiciones[4] == 1 && tri.posiciones[5] == 1)){
					Window.alert("Jugdor 1 gana!");
				}
				if((tri.posiciones[0] == 2 && tri.posiciones[3] == 2 && tri.posiciones[6] == 2)||
				   (tri.posiciones[3] == 2 && tri.posiciones[4] == 2 && tri.posiciones[5] == 2)){
					Window.alert("Jugdor 2 gana!");
				}
				else if(tri.draw()) Window.alert("Empate!");
			}
		});
		btn5.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				RootPanel.get("pos55").clear();
				if(tri.cambio_simbolo == 0){
					RootPanel.get("pos55").add(new HTML(x));
					tri.posiciones[4] = 1;
					tri.cambio_simbolo = 1;
				}
				else{
					RootPanel.get("pos55").add(new HTML(o));
					tri.posiciones[4] = 2;
					tri.cambio_simbolo = 0;
				}
				
				if((tri.posiciones[0] == 1 && tri.posiciones[4] == 1 && tri.posiciones[8] == 1)||
				   (tri.posiciones[1] == 1 && tri.posiciones[4] == 1 && tri.posiciones[7] == 1)||
				   (tri.posiciones[2] == 1 && tri.posiciones[4] == 1 && tri.posiciones[6] == 1)||
				   (tri.posiciones[3] == 1 && tri.posiciones[4] == 1 && tri.posiciones[5] == 1)){
					Window.alert("Jugdor 1 gana!");
				}
				if((tri.posiciones[0] == 2 && tri.posiciones[4] == 2 && tri.posiciones[8] == 2)||
				   (tri.posiciones[1] == 2 && tri.posiciones[4] == 2 && tri.posiciones[7] == 2)||
				   (tri.posiciones[2] == 2 && tri.posiciones[4] == 2 && tri.posiciones[6] == 2)||
				   (tri.posiciones[3] == 2 && tri.posiciones[4] == 2 && tri.posiciones[5] == 2)){
					Window.alert("Jugdor 2 gana!");
				}
				else if(tri.draw()) Window.alert("Empate!");
			}
		});
		btn6.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				RootPanel.get("pos66").clear();
				if(tri.cambio_simbolo == 0){
					RootPanel.get("pos66").add(new HTML(x));
					tri.posiciones[5] = 1;
					tri.cambio_simbolo = 1;
				}
				else{
					RootPanel.get("pos66").add(new HTML(o));
					tri.posiciones[5] = 2;
					tri.cambio_simbolo = 0;
				}
				
				if((tri.posiciones[2] == 1 && tri.posiciones[5] == 1 && tri.posiciones[8] == 1)||
				   (tri.posiciones[3] == 1 && tri.posiciones[4] == 1 && tri.posiciones[5] == 1)){
					Window.alert("Jugdor 1 gana!");
				}
				if((tri.posiciones[2] == 2 && tri.posiciones[5] == 2 && tri.posiciones[8] == 2)||
				   (tri.posiciones[3] == 2 && tri.posiciones[4] == 2 && tri.posiciones[5] == 2)){
					Window.alert("Jugdor 2 gana!");
				}
				else if(tri.draw()) Window.alert("Empate!");
			}
		});
		btn7.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				RootPanel.get("pos77").clear();
				if(tri.cambio_simbolo == 0){
					RootPanel.get("pos77").add(new HTML(x));
					tri.posiciones[6] = 1;
					tri.cambio_simbolo = 1;
				}
				else{
					RootPanel.get("pos77").add(new HTML(o));
					tri.posiciones[6] = 2;
					tri.cambio_simbolo = 0;
				}
				
				if((tri.posiciones[0] == 1 && tri.posiciones[3] == 1 && tri.posiciones[6] == 1)||
				   (tri.posiciones[6] == 1 && tri.posiciones[4] == 1 && tri.posiciones[2] == 1)||
				   (tri.posiciones[6] == 1 && tri.posiciones[7] == 1 && tri.posiciones[8] == 1)){
					Window.alert("Jugdor 1 gana!");
				}
				if((tri.posiciones[0] == 2 && tri.posiciones[3] == 2 && tri.posiciones[6] == 2)||
				   (tri.posiciones[6] == 2 && tri.posiciones[4] == 2 && tri.posiciones[2] == 2)||
				   (tri.posiciones[6] == 2 && tri.posiciones[7] == 2 && tri.posiciones[8] == 2)){
					Window.alert("Jugdor 2 gana!");
				}
				else if(tri.draw()) Window.alert("Empate!");
			}
		});
		btn8.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				RootPanel.get("pos88").clear();
				if(tri.cambio_simbolo == 0){
					RootPanel.get("pos88").add(new HTML(x));
					tri.posiciones[7] = 1;
					tri.cambio_simbolo = 1;
				}
				else{
					RootPanel.get("pos88").add(new HTML(o));
					tri.posiciones[7] = 2;
					tri.cambio_simbolo = 0;
				}
				
				if((tri.posiciones[1] == 1 && tri.posiciones[4] == 1 && tri.posiciones[7] == 1)||
				   (tri.posiciones[6] == 1 && tri.posiciones[7] == 1 && tri.posiciones[8] == 1)){
					Window.alert("Jugdor 1 gana!");
				}
				if((tri.posiciones[1] == 2 && tri.posiciones[4] == 2 && tri.posiciones[7] == 2)||
				   (tri.posiciones[6] == 2 && tri.posiciones[7] == 2 && tri.posiciones[8] == 2)){
					Window.alert("Jugdor 2 gana!");
				}
				else if(tri.draw()) Window.alert("Empate!");
			}
		});
		btn9.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				RootPanel.get("pos99").clear();
				if(tri.cambio_simbolo == 0){
					RootPanel.get("pos99").add(new HTML(x));
					tri.posiciones[8] = 1;
					tri.cambio_simbolo = 1;
				}
				else{
					RootPanel.get("pos99").add(new HTML(o));
					tri.posiciones[8] = 2;
					tri.cambio_simbolo = 0;
				}
				
				if((tri.posiciones[6] == 1 && tri.posiciones[7] == 1 && tri.posiciones[8] == 1)||
				   (tri.posiciones[0] == 1 && tri.posiciones[4] == 1 && tri.posiciones[8] == 1)||
				   (tri.posiciones[2] == 1 && tri.posiciones[5] == 1 && tri.posiciones[8] == 1)){
					Window.alert("Jugdor 1 gana!");
				}
				if((tri.posiciones[6] == 2 && tri.posiciones[7] == 2 && tri.posiciones[8] == 2)||
				   (tri.posiciones[0] == 2 && tri.posiciones[4] == 2 && tri.posiciones[8] == 2)||
				   (tri.posiciones[2] == 2 && tri.posiciones[5] == 2 && tri.posiciones[8] == 2)){
					Window.alert("Jugdor 2 gana!");
				}
				else if(tri.draw()) Window.alert("Empate!");
			}
		});
		l.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				RootPanel.get("pos11").clear();
				RootPanel.get("pos22").clear();
				RootPanel.get("pos33").clear();
				RootPanel.get("pos44").clear();
				RootPanel.get("pos55").clear();
				RootPanel.get("pos66").clear();
				RootPanel.get("pos77").clear();
				RootPanel.get("pos88").clear();
				RootPanel.get("pos99").clear();
				tri.cambio_simbolo = 0;
				tri.clear();
			}
		});
		//RootPanel.get("cajadetexto").add(textBox);
		RootPanel.get("nombre1").add(nom1);
		RootPanel.get("nombre2").add(nom2);
		RootPanel.get("pos1").add(btn1);
		RootPanel.get("pos2").add(btn2);
		RootPanel.get("pos3").add(btn3);
		RootPanel.get("pos4").add(btn4);
		RootPanel.get("pos5").add(btn5);
		RootPanel.get("pos6").add(btn6);
		RootPanel.get("pos7").add(btn7);
		RootPanel.get("pos8").add(btn8);
		RootPanel.get("pos9").add(btn9);
		RootPanel.get("limpiar").add(l);
	}
}
